# Netquest
## Frontend Technical Test

## Instructions

The purpose of this assignment is to see how you develop code. Regarding the code you must
develop, do it like it was on production, in other words, be worried about quality. Follow your own
coding conventions but make it coherent through all the code you develop. Write clean,
maintainable, quality code using all components you need.
The main goal of the test is to simulate the interaction between a user and an audience web
application where you have a set of input variables and a report containing a result set based on the
variables. For this test, you must focus on the user interface, helping the user get the best experience
to view audience information.

## To do
- [x] Create repo
- [x] Init Readme
- [x] ~~Start~~ Finish app

## Installation and Usage

After cloning the repository, run `npm install` to install dependencies; `npm start` to open a webpack-dev-server on port 3000


## Requirements

This project was bootstrapped with the new `create-react-app 2.0` so I can only recommend the latest version of `node` to date (`v10.11.0`).
It will fail to build with versions older than v9 (most probably)
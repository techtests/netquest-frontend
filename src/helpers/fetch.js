import audience from '../data/audience'
import available from '../data/available'

// Define our fake endpoints for the test
const endpoints = {
  audience,
  available
}

export const server = {
  AUDIENCE: 'audience',
  AVAILABLE: 'available'
}

export default {
  // Return a clean object, only item in data array
  get: endpoint => new Promise((resolve, reject) => { resolve(endpoints[endpoint].data) })
}

import React from 'react'
import { BarChart, Bar, XAxis, YAxis, Tooltip, Legend } from 'recharts'

export default ({ data, xDataKey, yDataKey }) => {
  if (!data) return null

  return (
    <BarChart
      width={640} height={480} data={data}
      margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
      <XAxis dataKey={xDataKey} interval={0} />
      <YAxis />
      <Tooltip />
      <Legend />
      <Bar dataKey={yDataKey} fill='aquamarine' />
    </BarChart>
  )
}

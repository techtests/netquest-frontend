import React, { Component } from 'react'
import DatePicker from 'react-datepicker'
import moment from 'moment'

class Toolbar extends Component {
  constructor (props) {
    super(props)

    this.state = {
      startDate: null,
      endDate: null,
      excludeDates: null,
      selectedDate: null,
      selectedIndex: null,
      selectedAttribute: null
    }
  }

  componentDidUpdate (prevProps) {
    if (prevProps === this.props) return
    if (!this.props.tools) return

    this.setState({
      startDate: moment(this.props.tools.dateFrom),
      endDate: moment(this.props.tools.dateTo),
      excludeDates: this.props.tools.unavailableDates.map(date => moment(date))
    })
  }

  selectOptions (options) {
    return options.map(item => (<option key={item.id} value={item.name}>{item.name}</option>))
  }

  render () {
    if (!this.props.tools) return null
    return (
      <form className='form-inline'>
        <select
          className='form-control mr-2'
          onChange={this.props.changes.index}
        >
          {this.selectOptions(this.props.tools.indexes)}
        </select>
        <select
          className='form-control mr-2'
          onChange={this.props.changes.attribute}
        >
          {this.selectOptions(this.props.tools.attributes)}
        </select>
        <DatePicker
          className='form-control mr-2 mb-1'
          minDate={this.state.startDate}
          maxDate={this.state.endDate}
          excludeDates={this.state.excludeDates}
          selected={this.props.values.date}
          onChange={this.props.changes.date}
        />
        <button
          className='btn btn-primary'
          onClick={this.props.submit}>
          Submit
        </button>
      </form>
    )
  }
}

export default Toolbar

import React, { Component } from 'react'
import moment from 'moment'
import fetch, { server } from '../helpers/fetch'

import Toolbar from './toolbar.component'
import Report from './report.component'

class Home extends Component {
  constructor (props) {
    super(props)

    this.state = {
      audience: null,
      available: null,
      index: null,
      attribute: null,
      date: null
    }

    this.onIndexChange = this.onIndexChange.bind(this)
    this.onAttributeChange = this.onAttributeChange.bind(this)
    this.onDateChange = this.onDateChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  componentDidMount () {
    fetch.get(server.AVAILABLE).then(
      data => {
        let result = data[0]
        this.setState({
          available: result,
          index: result.indexes[0].name,
          attribute: result.attributes[0].name,
          date: moment(result.dateFrom)
        })
      },
      error => console.log(error)
    )
  }

  onIndexChange (event) {
    this.setState({ index: event.target.value })
  }

  onAttributeChange (event) {
    this.setState({ attribute: event.target.value })
  }

  onDateChange (date) {
    this.setState({ date })
  }

  onSubmit (event) {
    event.preventDefault()
    fetch.get(server.AUDIENCE).then(
      result => {
        this.setState({
          audience: result
        })
      },
      error => console.log(error)
    )
  }

  renderReport () {
    if (!this.state.audience) return null
    return (
      <Report data={this.state.audience} yDataKey={this.state.index} xDataKey='Channel' />
    )
  }

  render () {
    return (
      <div>
        <Toolbar
          tools={this.state.available}
          values={{
            index: this.state.index,
            attribute: this.state.attribute,
            date: this.state.date
          }}
          changes={{
            index: this.onIndexChange,
            attribute: this.onAttributeChange,
            date: this.onDateChange
          }}
          submit={this.onSubmit}
        />
        { this.renderReport() }
      </div>
    )
  }
}

export default Home

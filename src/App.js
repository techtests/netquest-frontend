import React, { Component } from 'react'

import Home from './components/home.component'
import Report from './components/report.component'

import 'bootstrap/dist/css/bootstrap.min.css'
import 'react-datepicker/dist/react-datepicker.css'
import './App.css'

class App extends Component {
  render () {
    return (
      <div className='App'>
        <h1>Netquest</h1>
        <Home />
        <Report name={null} />
      </div>
    )
  }
}

export default App

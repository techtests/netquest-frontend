export default {
  'data': [{
    'Channel': 'All',
    'Target': 'Total Internet Users',
    'Visits (#)': 39901,
    'Page Views (#)': 102165,
    'Unique Users (#)': 15785
  },
  {
    'Channel': 'Peugeot.Es',
    'Target': 'Total Internet Users',
    'Visits (#)': 8749,
    'Page Views (#)': 34342,
    'Unique Users (#)': 8749
  },
  {
    'Channel': 'Misako.Com',
    'Target': 'Total Internet Users',
    'Visits (#)': 12957,
    'Page Views (#)': 14534,
    'Unique Users (#)': 12957
  },
  {
    'Channel': 'Interior.Gob.Es',
    'Target': 'Total Internet Users',
    'Visits (#)': 3852,
    'Page Views (#)': 3852,
    'Unique Users (#)': 3852
  },
  {
    'Channel': 'Bookdepository.Com',
    'Target': 'Total Internet Users',
    'Visits (#)': 14343,
    'Page Views (#)': 49437,
    'Unique Users (#)': 7353
  }]
}

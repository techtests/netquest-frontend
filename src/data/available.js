export default {
  'data': [{
    'indexes': [{
      'id': 1,
      'name': 'Visits (#)'
    },
    {
      'id': 2,
      'name': 'Page Views (#)'
    },
    {
      'id': 5,
      'name': 'Unique Users (#)'
    }],
    'attributes': [{
      'id': 3,
      'name': 'Target'
    }],
    'unavailableDates': ['2017-03-03', '2017-03-09', '2017-03-21'],
    'dateFrom': '2017-03-01',
    'dateTo': '2017-03-31'
  }],
  'errors': [],
  'httpStatus': 'OK'
}
